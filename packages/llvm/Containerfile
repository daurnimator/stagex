FROM scratch as base
ENV VERSION=16.0.6
ENV SRC_FILE=llvm-project-${VERSION}.src.tar.xz
ENV SRC_SITE=https://github.com/llvm/llvm-project/releases/download/llvmorg-${VERSION}/${SRC_FILE}
ENV SRC_HASH=ce5e71081d17ce9e86d7cbcfa28c4b04b9300f8fb7e78422b1feb6bc52c3028e

FROM base as fetch
ADD --checksum=sha256:${SRC_HASH} ${SRC_SITE} .

FROM fetch as build
COPY --from=stagex/busybox . /
COPY --from=stagex/binutils . /
COPY --from=stagex/cmake . /
COPY --from=stagex/ninja . /
COPY --from=stagex/musl . /
COPY --from=stagex/gcc . /
COPY --from=stagex/python . /
COPY --from=stagex/py-setuptools . /
COPY --from=stagex/zlib . /
COPY --from=stagex/openssl . /
# HACK: figure out why gcc package puts these in the wrong path at install time
COPY --from=stagex/gcc /usr/lib64/* /usr/lib/
RUN tar -xf ${SRC_FILE}
WORKDIR llvm-project-${VERSION}.src
ADD *.patch .
RUN --network=none <<-EOF
	set -eux
	python -c "import setuptools; print(setuptools.__version__)"
	cmake \
		-B build \
		-G Ninja \
		-Wno-dev -S llvm \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_INSTALL_PREFIX=/usr/ \
		-DCMAKE_INSTALL_RPATH=/usr/ \
		-DLLVM_DEFAULT_TARGET_TRIPLE="x86_64-linux-musl" \
		-DLLVM_HOST_TRIPLE="x86_64-linux-musl" \
		-DLLVM_APPEND_VC_REV=OFF \
		-DLLVM_BINUTILS_INCDIR=/usr/include \
		-DLLVM_BUILD_DOCS=OFF \
		-DLLVM_BUILD_EXAMPLES=OFF \
		-DLLVM_BUILD_EXTERNAL_COMPILER_RT=ON \
		-DLLVM_BUILD_LLVM_DYLIB=ON \
		-DLLVM_BUILD_TESTS=ON \
		-DLLVM_ENABLE_ASSERTIONS=OFF \
		-DLLVM_ENABLE_DUMP=ON \
		-DLLVM_ENABLE_EH=ON \
		-DLLVM_ENABLE_FFI=OFF \
		-DLLVM_ENABLE_LIBCXX=OFF \
		-DLLVM_ENABLE_LIBEDIT=OFF \
		-DLLVM_ENABLE_PIC=ON \
		-DLLVM_ENABLE_RTTI=ON \
		-DLLVM_ENABLE_SPHINX=OFF \
		-DLLVM_ENABLE_TERMINFO=ON \
		-DLLVM_ENABLE_ZLIB=OFF \
		-DLLVM_ENABLE_ZSTD=OFF \
		-DLLVM_INCLUDE_BENCHMARKS=OFF \
		-DLLVM_INCLUDE_EXAMPLES=OFF \
		-DLLVM_INSTALL_UTILS=ON \
		-DLLVM_LINK_LLVM_DYLIB=ON \
		-DLLVM_USE_PERF=ON
	cmake --build build
	python3 llvm/utils/lit/setup.py build
EOF

FROM build as install
RUN <<-EOF
	set -eux
	DESTDIR="/rootfs" cmake --install build
	python3 llvm/utils/lit/setup.py install --root="/rootfs"
	ln -s lit /rootfs/usr/bin/llvm-lit
EOF
RUN find /rootfs -exec touch -hcd "@0" "{}" +

FROM scratch as package
COPY --from=install /rootfs /

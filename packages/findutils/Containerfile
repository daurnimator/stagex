FROM scratch as base
ARG ARCH=x86_64
ENV VERSION=4.9.0
ENV SRC_HASH=a2bfb8c09d436770edc59f50fa483e785b161a3b7b9d547573cb08065fd462fe
ENV SRC_FILE=findutils-${VERSION}.tar.xz
ENV SRC_SITE=https://ftp.gnu.org/gnu/findutils/${SRC_FILE}

FROM base as fetch
ADD --checksum=sha256:${SRC_HASH} ${SRC_SITE} .

FROM fetch as build
COPY --from=stagex/busybox . /
COPY --from=stagex/binutils . /
COPY --from=stagex/musl . /
COPY --from=stagex/make . /
COPY --from=stagex/gcc . /
RUN tar -xf ${SRC_FILE}
WORKDIR findutils-${VERSION}
RUN --network=none <<-EOF
	set -ex
	./configure \
		--build=${ARCH}-linux-musl \
		--host=${ARCH}-linux-musl \
		--prefix=/usr \
		--sysconfdir=/etc \
		--localstatedir=/var \
		--mandir=/usr/share/man \
        --infodir=/usr/share/info \
        gl_cv_func_getcwd_path_max="no, but it is partly working"
	make -j "$(nproc)"
EOF

FROM build as install
RUN --network=none make DESTDIR="/rootfs" install
RUN find /rootfs -exec touch -hcd "@0" "{}" +

FROM scratch as package
COPY --from=install /rootfs /

FROM scratch as base
ENV VERSION=6.6
ENV SRC_HASH=d926a06c63dd8ac7df3f86ee1ffc2ce2a3b81a2d168484e76b5b389aba8e56d0
ENV SRC_FILE=linux-${VERSION}.tar.xz
ENV SRC_SITE=https://mirrors.edge.kernel.org/pub/linux/kernel/v6.x/${SRC_FILE}

FROM base as fetch
ADD --checksum=sha256:${SRC_HASH} ${SRC_SITE} .

FROM fetch as build
COPY --from=stagex/stage3 . /
RUN tar -xf linux-${VERSION}.tar.xz
WORKDIR linux-${VERSION}
RUN --network=none make headers

FROM build as install
RUN <<-EOF
	set -eux
	mkdir -p /rootfs/usr
	cp -a usr/include /rootfs/usr/
	find /rootfs/usr/include/ ! -iname "*.h" -type f -exec rm -v {} \+
	rm -rf /rootfs/usr/include/drm
EOF
RUN find /rootfs -exec touch -hcd "@0" "{}" +

FROM scratch as package
COPY --from=install /rootfs /
